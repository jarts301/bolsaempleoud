﻿//var ServiceURL = "http://192.168.0.3:8080/";
var ServiceURL = "http://54.88.222.130:8080/BolsaEmpleoUDBackEnd/";

function usuarioLogIn(document) {
    event.preventDefault();
    var parametros = $("#formLogIn").serialize().replace(/\b(=&)\b/gi, '&');//.replace(/\b(\+)\b/gi, ' ');
    //alert(parametros);
    $.getJSON(ServiceURL + "usuario_obtenerPorEmailPass?" + parametros, function (data) {
        if (data.nombre != null) {
            localStorage.setItem("nombre", data.nombre);
            localStorage.setItem("rol", data.rol);
            localStorage.setItem("id", data.id);
            if (data.rol == "Aspirante") {
                window.location = "listaOfertas.html";
            }
            if (data.rol == "Empresa") {
                window.location = "regOferta.html";
            }
        } else {
            alert("Correo o contraseña incorrectos.");
        }
    });
}

function sectores(document) {
    var selectSector = document.getElementById("filtroOfertas");

    $.getJSON(ServiceURL + "sector_obtenerTodos", function (data) {
        for (var i = 0; i < data.length; i++){
            var sector = document.createElement("option");
            sector.value = data[i].id;
            sector.text = data[i].nombre;
            selectSector.add(sector);
        }
    });
}

function sectores2(document) {
    var selectSector = document.getElementById("sector");

    $.getJSON(ServiceURL + "sector_obtenerTodos", function (data) {
        for (var i = 0; i < data.length; i++) {
            var sector = document.createElement("option");
            sector.value = data[i].id;
            sector.text = data[i].nombre;
            selectSector.add(sector);
        }
    });
}

function oferta(document, ofertaId) {
    $.getJSON(ServiceURL + "oferta_obtenerPorId?ofertaId="+ofertaId, function (data) {
        var titulo = document.getElementById("titulo");
        var salario = document.getElementById("salario");
        var fechaInicial = document.getElementById("fechaInicial");
        var fechaFinal = document.getElementById("fechaFinal");
        var pais = document.getElementById("pais");
        var ciudad = document.getElementById("ciudad");
        var descripcion = document.getElementById("descripcion");

        titulo.innerHTML = data.cargo;
        salario.innerHTML = "$"+data.salario;
        fechaInicial.innerHTML = data.fechaInicial.substring(0, 11);
        fechaFinal.innerHTML = data.fechaFinal.substring(0, 11);
        pais.innerHTML = data.pais;
        ciudad.innerHTML = data.ciudad;
        descripcion.innerHTML = data.descripcion;
    });
}

function listaOfertas(document, sector){
    var listado = document.getElementById("listado");
    while (listado.firstChild) {
        listado.removeChild(listado.firstChild);
    }

    $.getJSON(ServiceURL + "oferta_obtenerPorSector?sectorId=" + sector, function (data) {

        for (var i = 0; i < data.length; i++) {
            var oferta = document.createElement("div");
            oferta.className = "col-sm-12 text-center";
            oferta.style.backgroundColor = (i % 2 == 0 ? "#efefef" : "#ffffff");
            oferta.id = data[i].id;
            oferta.addEventListener("click", function () {
                cambiarColor(this);
                irAPagina("oferta.html?id=" + this.id);
            }, false);

            var titulo = document.createElement("p");
            titulo.className = "text-left";
            titulo.style.fontSize = "16px";
            titulo.style.fontWeight = "bold";
            titulo.style.marginBottom = "1px";
            titulo.innerHTML = data[i].cargo;

            var salario = document.createElement("p");
            salario.className = "text-left";
            salario.style.fontSize = "15px";
            salario.style.marginTop = "1px";
            salario.style.marginBottom = "1px";
            salario.innerHTML = "Salario: $" + data[i].salario;

            var descripcion = document.createElement("p");
            descripcion.className = "text-left";
            descripcion.style.fontSize = "15px";
            descripcion.style.marginTop = "1px";
            descripcion.innerHTML = data[i].descripcion.substring(0,80) +"...";

            oferta.appendChild(titulo);
            oferta.appendChild(salario);
            oferta.appendChild(descripcion);

            listado.appendChild(oferta);
        }
        //$('#listado').listview('refresh');
    });

}

function experienciaUsuario(document) {
    var listado = document.getElementById("listado");
    while (listado.firstChild) {
        listado.removeChild(listado.firstChild);
    }
    var usuarioId = localStorage.getItem("id");

    $.getJSON(ServiceURL + "experiencia_obtenerPorUsuario?usuarioId=" + usuarioId, function (data) {

        for (var i = 0; i < data.length; i++) {
            var experiencia = document.createElement("div");
            experiencia.className = "col-sm-12 text-left";
            experiencia.style.backgroundColor = (i % 2 == 0 ? "#efefef" : "#ffffff");

            var empresa = document.createElement("label");
            empresa.innerHTML = "Empresa: " + data[i].empresa;
            var cargo = document.createElement("label");
            cargo.innerHTML = "Cargo: " + data[i].cargo;
            var inicioFin = document.createElement("label");
            inicioFin.innerHTML = "Inicio y Fin: " + data[i].fechaInicio.substring(0, 11) + " / " + data[i].fechaFin.substring(0, 11);
            var contacto = document.createElement("label");
            contacto.innerHTML = "Contacto: " + data[i].contacto;
            var telContacto = document.createElement("label");
            telContacto.innerHTML = "Teléfono Contacto: " + data[i].telContacto;
            var funciones = document.createElement("label");
            funciones.innerHTML = "Funciones: " + data[i].funciones;

            var eliminar = document.createElement("a");
            eliminar.innerHTML = "Eliminar";
            eliminar.href = "#";
            eliminar.id = data[i].id;
            eliminar.addEventListener("click", function () {
                eliminarExperiencia(document,this.id);
            }, false);

            experiencia.appendChild(empresa);
            experiencia.appendChild(document.createElement("br"));
            experiencia.appendChild(cargo);
            experiencia.appendChild(document.createElement("br"));
            experiencia.appendChild(inicioFin);
            experiencia.appendChild(document.createElement("br"));
            experiencia.appendChild(contacto);
            experiencia.appendChild(document.createElement("br"));
            experiencia.appendChild(telContacto);
            experiencia.appendChild(document.createElement("br"));
            experiencia.appendChild(funciones);
            experiencia.appendChild(document.createElement("br"));
            experiencia.appendChild(eliminar);

            listado.appendChild(experiencia);
        }
    });

}

function estudioUsuario(document) {
    var listado = document.getElementById("listado");
    while (listado.firstChild) {
        listado.removeChild(listado.firstChild);
    }
    var usuarioId = localStorage.getItem("id");

    $.getJSON(ServiceURL + "estudio_obtenerPorUsuario?usuarioId=" + usuarioId, function (data) {

        for (var i = 0; i < data.length; i++) {
            var estudio = document.createElement("div");
            estudio.className = "col-sm-12 text-left";
            estudio.style.backgroundColor = (i % 2 == 0 ? "#efefef" : "#ffffff");

            var nombre = document.createElement("label");
            nombre.innerHTML = "Nombre: " + data[i].nombre;
            var inicioFin = document.createElement("label");
            inicioFin.innerHTML = "Inicio y Fin: " + data[i].inicio.substring(0, 11) + " / " + data[i].terminacion.substring(0, 11);
            var tipo = document.createElement("label");
            tipo.innerHTML = "Tipo: " + data[i].tipo;
            var institucion = document.createElement("label");
            institucion.innerHTML = "Institución: " + data[i].institucion;

            var eliminar = document.createElement("a");
            eliminar.innerHTML = "Eliminar";
            eliminar.href = "#";
            eliminar.id = data[i].id;
            eliminar.addEventListener("click", function () {
                eliminarEstudio(document, this.id);
            }, false);

            estudio.appendChild(nombre);
            estudio.appendChild(document.createElement("br"));
            estudio.appendChild(inicioFin);
            estudio.appendChild(document.createElement("br"));
            estudio.appendChild(tipo);
            estudio.appendChild(document.createElement("br"));
            estudio.appendChild(institucion);
            estudio.appendChild(document.createElement("br"));
            estudio.appendChild(eliminar);

            listado.appendChild(estudio);
        }
    });

}

function eliminarExperiencia(document, experienciaId) {
    var r = confirm("¿Seguro que desea eliminar la experiencia?");
    if (r == true) {
        $.getJSON(ServiceURL + "experiencia_eliminar?experienciaId=" + experienciaId, function (data) {
            window.location = "experiencia.html";
        });
    }
}

function eliminarEstudio(document, estudioId) {
    var r = confirm("¿Seguro que desea eliminar el logro académico?");
    if (r == true) {
        $.getJSON(ServiceURL + "estudio_eliminar?estudioId=" + estudioId, function (data) {
            window.location = "academico.html";
        });
    }
}


function consultarUsuario(document) {
    var usuarioId = localStorage.getItem("id");

    $.getJSON(ServiceURL + "usuario_obtenerPorId?usuarioId=" + usuarioId, function (data) {
        document.getElementById("op" + data.rol).selected = "true";
        document.getElementById("rol").disabled = "true";
        document.getElementById("nombre").value = data.nombre;
        document.getElementById("email").value = data.email;
        document.getElementById("rol").disabled = "true";
        document.getElementById("password").value = data.pass;
        document.getElementById("op" + data.tipoDoc).selected = "true";
        document.getElementById("documento").value = data.doc;
        document.getElementById("telefono").value = data.telefono;
        document.getElementById("perfil").innerHTML = data.perfil;
        document.getElementById("pais").value = data.pais;
        document.getElementById("ciudad").value = data.ciudad;
        document.getElementById("libreta").value = data.libreta != null ? data.libreta : "";
        document.getElementById("direccion").value = data.direccion != null ? data.direccion : "";
        document.getElementById("paginaWeb").value = data.web != null ? data.web : "";
        if (data.genero != null) { document.getElementById("op" + data.genero).selected = "true"; }
        document.getElementById("estadoCivil").value = data.estadoCivil != null ? data.estadoCivil : "";
        document.getElementById("licencia").value = data.licenciaCond != null ? data.licenciaCond : "";
        if (data.catLicenciaCond != null)
        {
            if (data.catLicenciaCond == "Sin licencia") {
                document.getElementById("opSinLicencia").selected = "true";
            } else {
                document.getElementById("op" + data.catLicenciaCond).selected = "true";
            }
        }
    });

}

function enviarMailAEmpresa(document, ofertaId) {
    var usuarioId = localStorage.getItem("id");

    $.getJSON(ServiceURL + "aplicacion_mailAEmpresa?ofertaId=" + ofertaId +"&aspiranteId="+usuarioId, function (data) {
        //Correo enviado
    });

}

function descargarHV(document,usuarioId) {
    var ref = cordova.InAppBrowser.open(ServiceURL + 'aplicacion_descargarHV?usuarioId=' + usuarioId, '_system', 'location=no,hidden=yes');
}

function verificarHV(document) {

    var usuarioId = localStorage.getItem("id");

    $.getJSON(ServiceURL + "aplicacion_verificarHV?usuarioId=" + usuarioId, function (data) {
        if (data.nombre != null) {
            document.getElementById("nombreHV").innerHTML = data.nombre + "." + data.extension;
            document.getElementById("nombreHV").style.color = "#0036a5";
            document.getElementById("nombreHV").addEventListener("click", function () {
                descargarHV(document, localStorage.getItem("id"));
            }, false);
        }
    });

    /*var fileTransfer = new FileTransfer();
    var uri = encodeURI(ServiceURL + "aplicacion_bajarHV?usuarioId=" + localStorage.getItem("id"));
    var fileURL = cordova.file.dataDirectory+"hojaVida.pdf";

    fileTransfer.download(
        uri,
        fileURL,
        function (entry) {
            console.log("download complete: " + entry.toURL());
        },
        function (error) {
            console.log("download error source " + error.source);
            console.log("download error target " + error.target);
            console.log("download error code" + error.code);
        },
        false,
        {
            headers: {
                "usuarioId": localStorage.getItem("id")
            }
        }
    );*/

    //alert(cordova.file.dataDirectory);

    /*var success = function (fileSystem) {
        //console.log(fileSystem.name);
        alert(fileSystem.name);
    }
    var error = function (evt) {
        //console.log(evt.target.error.code);
        alert("No se puede guardar el archivo.");
        //console.log(fileSystem.name);
    }

    var successFile = function (fileEntry) {
        console.log(fileEntry.name);
        alert(fileEntry.name);
    }
    
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, success, error);
    window.resolveLocalFileSystemURL(cordova.file.dataDirectory, successFile, error);
    */

    /*var usuarioId = localStorage.getItem("id");

    $.getJSON(ServiceURL + "aplicacion_bajarHV?usuarioId=" + usuarioId, function (data) {
        
    });*/
}