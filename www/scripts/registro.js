﻿//var ServiceURL = "http://192.168.0.3:8080/";
var ServiceURL = "http://54.88.222.130:8080/BolsaEmpleoUDBackEnd/";

function registrarUsuario(document) {
    event.preventDefault();
    var parametros = $("#formRegistro").serialize().replace(/\b(=&)\b/gi, '&');//.replace(/\b(\+)\b/gi, ' ');
    //alert(parametros);
    $.getJSON(ServiceURL + "usuario_registrar?" + parametros, function (data) {
        var id = data.num;
        window.location = "index.html";
    });
}

function registrarExperiencia(document) {
    event.preventDefault();
    var usuarioId = document.getElementById("usuarioId");
    usuarioId.value = localStorage.getItem("id");
    var parametros = $("#formRegistro").serialize().replace(/\b(=&)\b/gi, '&');
    $.getJSON(ServiceURL + "experiencia_registrar?" + parametros, function (data) {
        var id = data.num;
        window.location = "experiencia.html";
    });
}

function registrarEstudio(document) {
    event.preventDefault();
    var usuarioId = document.getElementById("usuarioId");
    usuarioId.value = localStorage.getItem("id");
    var parametros = $("#formRegistro").serialize().replace(/\b(=&)\b/gi, '&');
    $.getJSON(ServiceURL + "estudio_registrar?" + parametros, function (data) {
        var id = data.num;
        window.location = "academico.html";
    });
}

function registrarHV(document) {
    /*event.preventDefault();
    var usuarioId = document.getElementById("usuarioIdM");
    usuarioId.value = localStorage.getItem("id");*/
    /*var datos = document.getElementById("formArchivo");
    //alert(datos);
    var parametros = new FormData(datos);
    $.post(ServiceURL + "aplicacion_subirHV", parametros, function (data) {
        window.location = "academico.html";
    });*/


    window.plugins.mfilechooser.open(['.doc', '.docx', '.pdf'], function (uri) {

        //alert(uri);
        var usuarioId = localStorage.getItem("id");
        var fileURL = uri;

        var win = function (r) {
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
            alert("Hoja de vida Guardada correctamente.");
            window.location = "datosUsuario.html";
        }

        var fail = function (error) {
            alert("No se pudo subir el archivo, verifica que el tamaño sea menor a 1MB, que la ruta no es muy larga y que el archivo no esta dañado.");
            console.log("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
        }

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = "file"; //fileURL.substr(fileURL.lastIndexOf('/') + 1);
        //options.mimeType = "text/plain";

        var params = {};
        params.nombre = usuarioId + fileURL.substr(fileURL.lastIndexOf('.'));
        //params.value2 = "param";

        options.params = params;

        var ft = new FileTransfer();

        if (fileURL.substr(fileURL.lastIndexOf('.') + 1) == "pdf" ||
            fileURL.substr(fileURL.lastIndexOf('.') + 1) == "doc" ||
            fileURL.substr(fileURL.lastIndexOf('.') + 1) == "docx") {

            ft.upload(fileURL, encodeURI(ServiceURL + "aplicacion_subirHV"), win, fail, options);

        } else {
            alert("No es un formato de archivo permitido.");
        }

    }, function (error) {

        alert(error);

    });


    /*window.OurCodeWorld.Filebrowser.filePicker.single({
        success: function (data) {
            if (!data.length) {
                // No file selected
                return;
            }

            var usuarioId = localStorage.getItem("id");
            var fileURL = data[0];

            var win = function (r) {
                console.log("Code = " + r.responseCode);
                console.log("Response = " + r.response);
                console.log("Sent = " + r.bytesSent);
            }

            var fail = function (error) {
                alert("An error has occurred: Code = " + error.code);
                console.log("upload error source " + error.source);
                console.log("upload error target " + error.target);
            }

            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.fileName = "file"; //fileURL.substr(fileURL.lastIndexOf('/') + 1);
            //options.mimeType = "text/plain";

            var params = {};
            params.nombre = usuarioId+fileURL.substr(fileURL.lastIndexOf('.'));
            //params.value2 = "param";

            options.params = params;

            var ft = new FileTransfer();

            if (fileURL.substr(fileURL.lastIndexOf('.') + 1) == "pdf" ||
                fileURL.substr(fileURL.lastIndexOf('.') + 1) == "doc" ||
                fileURL.substr(fileURL.lastIndexOf('.') + 1) == "docx")
            {
                ft.upload(fileURL, encodeURI(ServiceURL + "aplicacion_subirHV"), win, fail, options);
                alert("Hoja de vida Guardada correctamente.");
            } else {
                alert("No es un formato de archivo permitido.");
            }


            console.log(data);

            // Array with the file path
            // ["file:///storage/emulated/0/360/security/file.txt"]
        },
        error: function (err) {
            console.log(err);
        }
    });*/


    /*var fileURL = document.getElementById("archivo").value;

    var win = function (r) {
        console.log("Code = " + r.responseCode);
        console.log("Response = " + r.response);
        console.log("Sent = " + r.bytesSent);
    }

    var fail = function (error) {
        alert("An error has occurred: Code = " + error.code);
        console.log("upload error source " + error.source);
        console.log("upload error target " + error.target);
    }

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
    options.mimeType = "text/plain";

    var params = {};
    params.value1 = "test";
    params.value2 = "param";

    options.params = params;

    var ft = new FileTransfer();
    ft.upload(fileURL, encodeURI(ServiceURL + "aplicacion_subirHV"), win, fail, options);*/

}

function editarUsuario(document) {
    event.preventDefault();
    var usuarioId = localStorage.getItem("id");
    document.getElementById("usuarioId").value=usuarioId;
    var parametros = $("#formEdicion").serialize().replace(/\b(=&)\b/gi, '&');
    $.getJSON(ServiceURL + "usuario_editar?" + parametros, function (data) {
        alert("Datos Actualizados");
    });
}

function registrarAplicacion(document,ofertaId) {
    event.preventDefault();
    var usuarioId = localStorage.getItem("id");
    $.getJSON(ServiceURL + "aplicacion_registrar?usuarioId="+usuarioId+"&ofertaId="+ofertaId, function (data) {
        var id = data.num;
        alert("Su aplicación sera analizada por la empresa.\nGracias");
    });
}

function recordarPassword(document) {
    event.preventDefault();
    var parametros = $("#formPassword").serialize().replace(/\b(=&)\b/gi, '&');
    $.getJSON(ServiceURL + "usuario_recuperarPassword?" + parametros, function (data) {
        window.location = "index.html";
    });
}

function registrarOferta(document) {
    event.preventDefault();
    var usuarioId = document.getElementById("usuarioId");
    usuarioId.value = localStorage.getItem("id");
    var parametros = $("#formRegistro").serialize().replace(/\b(=&)\b/gi, '&');
    $.getJSON(ServiceURL + "oferta_registrar?" + parametros, function (data) {
        alert("Oferta publicada exitosamente.");
        window.location = "regOferta.html";
    });
}



