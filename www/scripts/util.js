﻿var ServiceURL = 'http://192.168.0.4:8080/';

function getVarsUrl() {
    var url = location.search.replace("?", "");
    var arrUrl = url.split("&");
    var urlObj = {};
    for (var i = 0; i < arrUrl.length; i++) {
        var x = arrUrl[i].split("=");
        urlObj[x[0]] = x[1]
    }
    return urlObj;
}

function getServicio(servicio) {
    return ServiceURL + servicio;
}

function cerrarSesion(document) {
    localStorage.clear();
    window.location = "index.html";
}

function verificarSesion(document) {
    if (localStorage.getItem("id") != null) {
        if (localStorage.getItem("rol") == "Aspirante") {
            window.location = "listaOfertas.html";
        }
        if (localStorage.getItem("rol") == "Empresa") {
            window.location = "regOferta.html";
        }
    }
}

function cambioRol(document, rol) {
    if (rol.value == 'Aspirante') {
        document.getElementById('grupoLibreta').style.display='block';
        document.getElementById('grupoGenero').style.display = 'block';
        document.getElementById('grupoEstadoCivil').style.display = 'block';
        document.getElementById('grupoLicencia').style.display = 'block';
        document.getElementById('grupoCatLicencia').style.display = 'block';
        document.getElementById('opcionCC').selected = 'true';
        //document.getElementById('inputTipoDocumento').removeAttribute("disabled");
    }
    if (rol.value == 'Empresa') {
        document.getElementById('grupoLibreta').style.display = 'none';
        document.getElementById('grupoGenero').style.display = 'none';
        document.getElementById('grupoEstadoCivil').style.display = 'none';
        document.getElementById('grupoLicencia').style.display = 'none';
        document.getElementById('grupoCatLicencia').style.display = 'none';
        document.getElementById('opcionNIT').selected = 'true';
        //document.getElementById('inputTipoDocumento').disabled = 'true';
    }
}

function definirRol(document) {
    var rol = localStorage.getItem("rol");
    //var usuarioId = localStorage.getItem("rol");
    //document.getElementById("usuarioIdM").value = usuarioId;
    if (rol == 'Aspirante') {
        document.getElementById('grupoLibreta').style.display = 'block';
        document.getElementById('grupoGenero').style.display = 'block';
        document.getElementById('grupoEstadoCivil').style.display = 'block';
        document.getElementById('grupoLicencia').style.display = 'block';
        document.getElementById('grupoCatLicencia').style.display = 'block';
        document.getElementById('Titulo').innerHTML = "MI HOJA DE VIDA";
        document.getElementById('extraHojaVida').style.display = "block";
    }
    if (rol == 'Empresa') {
        document.getElementById('grupoLibreta').style.display = 'none';
        document.getElementById('grupoGenero').style.display = 'none';
        document.getElementById('grupoEstadoCivil').style.display = 'none';
        document.getElementById('grupoLicencia').style.display = 'none';
        document.getElementById('grupoCatLicencia').style.display = 'none';
        document.getElementById('Titulo').innerHTML = "PERFIL EMPRESARIAL";
        document.getElementById('extraHojaVida').style.display = "none";
    }
}

function opcionesMenuRol(document) {
    var rol = localStorage.getItem("rol");
    if (rol == 'Aspirante') {
        document.getElementById('menuListaOfertas').style.display = "block";
        document.getElementById('menuRegOferta').style.display = "none";
        document.getElementById('menuHojaVida').style.display = "block";
        document.getElementById('menuPerfEmpresa').style.display = "none";
    }
    if (rol == 'Empresa') {
        document.getElementById('menuListaOfertas').style.display = "none";
        document.getElementById('menuRegOferta').style.display = "block";
        document.getElementById('menuHojaVida').style.display = "none";
        document.getElementById('menuPerfEmpresa').style.display = "block";
    }
}

function verificarValor(input) {
    var nombre = input.id;
    var valor = input.value;
    if (valor != null && valor != "") {
        return nombre + "=" + valor;
    }
    return null;
}

function irAPagina(pagina) {
    window.location = pagina;
}

function cambiarColor(fila) {
    fila.style.backgroundColor = "#BCE0FF";
}